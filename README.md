# Ml model Canary deployment 

Accompanying repo for the article "Automatic Canary Releases for Machine Learning Models". 

## Content Description 

* `template.yml`. CloudFormation template to provide AWS resources. 

* `gitlab-ci.yml`. CD pipeline to continuosly deploy changes. 

* `canary`. On the canary folder one can find the `ml-canary-deploy.py` script to update a lambda function. This is turned into a lambda function by the CloudFormation resource `MLModelCanaryDeploy`. 

* `lambda`. The lambda folder contains the `ml-model-api.py` script that holds a sample prediction model. This is the machine learning model that you want to update in canary way.


## How to run the canary deploy script?

The `ml-canary-deploy.py` is a lambda function that one can invoke
```
aws lambda invoke --function-name ${LAMBDAFUNCTION%\"}
```
providing the following parameters
```
'{"alias":"PROD", "steps":5, "interval":10, "type":"linear"}'
```
where 
* `alias` is the alias of the lambda function that we want to update. In this example, we want to update the `PROD` alias of the `MLModelAPI` lambda function.
* `steps` is the number of steps that we take to update the function.
* `interval`. Every time a new weight is assigned, the time that you wait for the next weight to be assigned, this parameter goes to the `time.sleep()` function. 
* `type`. How to update the weights? Currently it only supports linear way, that is assign the same weight in every step.

See line 42 of `gitlab-ci.yml` file for more details on how to invoke this lambda function.

## How to activate the deploy stage?

As I'm following [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) structure, every time you deploy new changes you should follow a pattern 

```
Type (Api): Some commit message
```
where `Type` can be `Feat, Fix, Test`. The regex pattern matches a capital letter followed by two or more lower case letter. So, this will show the `deploy` stage on the pipelines section of the repo. The cogwheel symbol on the image below represents the `deploy` stage.

![cogwheel](build-deploy_stages.png)


## Acknowledgment

The canary deployment approach would not be possible if I had no stumble upon a sample script from AWS, that can be found [here](https://github.com/aws-samples/aws-lambda-deploy/blob/master/functions/simple/simple.py).