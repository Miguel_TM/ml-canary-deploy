from datetime import datetime, timedelta

import boto3
import json
import logging
import sys
import os

# Code based on aws sample
# https://github.com/aws-samples/aws-lambda-deploy/blob/master/functions/simple/simple.py


def init_logger():
    """
        Initialize a Logger that prints to the console following 
        the format %m/%d/%Y %I:%M:%S to indicate event's datetime

        Returns
        -------
        logger: A formatted logger.
    """

    # Initialize Logger and set Level to INFO
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Initialize a Handler to print to stdout
    handler = logging.StreamHandler(sys.stdout)

    # Format Handler output
    logFormatter = logging.Formatter(
        "%(asctime)s %(message)s", datefmt="%m/%d/%Y %I:%M:%S %p"
    )
    handler.setFormatter(logFormatter)

    # Set Handler Level to INFO
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    return logger


def check_cloudwatch_errors(func_name, alias_name, new_version, log) -> bool:
    """
        Check if lambda func_name + alias_name has error metrics
        in previous 5 minutes.

        Parameters
        ---------
        func_name: str
            Lambda function name
        
        alias_name: str
            Lambda function alias

        new_version:str
            The new version name of published lambda function


        Returns
        -------
        result: bool
            Wheter or not Cloudwatch errors were found
    """

    client = boto3.client("cloudwatch")

    func_plus_alias = func_name + ":" + alias_name
    now = datetime.utcnow()
    start_time = now - timedelta(minutes=5)

    response = client.get_metric_statistics(
        Namespace="AWS/Lambda",
        MetricName="Errors",
        Dimensions=[
            {"Name": "FunctionName", "Value": func_name},
            {"Name": "Resource", "Value": func_plus_alias},
            {"Name": "ExecutedVersion", "Value": new_version},
        ],
        StartTime=start_time,
        EndTime=now,
        Period=60,
        Statistics=["Sum"],
    )
    datapoints = response["Datapoints"]
    for datapoint in datapoints:
        if datapoint["Sum"] > 0:
            log.info(
                f"Warning -- Failing CloudWatch check: Error metrics found for new version: {datapoints}"
            )
            return False

    return True


def finalize(client, func_name, alias_name, version):
    """
       Set the new published version as the primary
       version.

       Params
       ------
       client: boto3.client
           Boto3 lambda client

       func_name: str
           Lambda function name

       alias_name: str
           Lambda function alias name

       version: str
           Lambda function published version
           to se as primary version.

        Returns
        -------
        res: boto3 client with updated alias
    """

    routing_config = {"AdditionalVersionWeights": {}}

    res = client.update_alias(
        FunctionName=func_name,
        FunctionVersion=version,
        Name=alias_name,
        RoutingConfig=routing_config,
    )

    return res


def generate_weights(type, steps):
    """
       Generate n linear stepts to update
       lambda function new version weight.

       Params
       ------
       type: str
           Function type to increment
           weights.

       steps: int
           Number of steps taken to 
           update weights completely.

        Returns
        -------
        values: list
            List with interval steps
    """

    if type == "linear":
        values = linear(steps)
    else:
        raise Exception("Invalid function type: " + type)
    return values


def linear(num_points):
    delta = 1.0 / num_points
    prev = 0
    values = []
    for _ in range(0, num_points):
        val = prev + delta
        values.append(round(val, 2))
        prev = val
    return values


def rollback(client, func_name, alias_name, log):
    """
       If CloudWatch Error metric appears generate rollback
       by routing all traffic to original version.

       Params
       ------
       client: boto3.client
            Boto3 lambda client

       func_name: str
            Lambda function name

       alias_name: str
            Lambda alias name

       log: Logger
    """

    log.Warning(
        "Warning -- Check CloudWatch Errors failed. Rolling back to original version"
    )

    routing_config = {"AdditionalVersionWeights": {}}
    client.update_alias(
        FunctionName=func_name, Name=alias_name, RoutingConfig=routing_config
    )
    log.info("Info -- Alias was successfully rolled back to original version")

    return


def update_weight(client, func_name, alias_name, version, next_weight, log):
    """
       Update Weights to new lambda version in type (ex:linear) way

       Params
       ------
       client: boto3.client
            Boto3 lambda client

       func_name: str
            Lambda function name

       alias_name: str
            Lambda alias name

       version: str
            Lambda function new published version

       next_weight: int
            New weight assigned to new version

       log: Logger
    """

    log.info(
        "Updating weight of alias {1}:{2} for version {0} to {3}".format(
            version, func_name, alias_name, next_weight
        )
    )

    weights = {version: next_weight}
    routing_config = {"AdditionalVersionWeights": weights}

    client.update_alias(
        FunctionName=func_name, Name=alias_name, RoutingConfig=routing_config
    )
    return


def validate_lambda_alias(client, file_name, alias_name):
    '''
       Call specified alias of lambda function with 
       a sample payload.

       Params
       ------
       file_name: str
            File name with sample payload.
            
       alias_name: str
            Lambda alias name

        Returns
        -------
        payload: dict
            Response payload
    '''

    # Load sample request event
    with open(file_name) as f:
        payload = json.load(f)

    # Test alias from lambda function
    response = client.invoke(
        FunctionName=f'{os.environ["function"]}:DEV',
        Payload=json.dumps(payload).encode(),
    )

    return json.loads(response["Payload"].read())
