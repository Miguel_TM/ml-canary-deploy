from utils import * 
import json
import boto3
import os
import time


def lambda_handler(event, context):

    # Initialize logger
    log = init_logger()

    # Retrieve event parameters
    steps = event.get('steps', 10)
    interval = event.get('interval', 30)
    weight_function = event.get('type', 'linear')

    # Initialize lambda client
    lambda_client = boto3.client('lambda')

    alias = 'DEV'
    log.info(f'Info -- Validating {alias} for Lambda: {os.environ["function"]}')

    # Validate DEV alias
    response = validate_lambda_alias(lambda_client, 'request.json', alias)
    
    # Retrieve status and message
    status = response['statusCode'] 
    message = json.loads(response['body'])

    log.info(f'Info -- Status code: {status}')
    log.info(f'Info -- Message: {message}')


    if status == 200:

        log.info('Info -- Publishing new lambda version')

        # Publish new version
        new_version = lambda_client.publish_version(FunctionName=os.environ['function'])[
            'Version'
        ]

        log.info('Info -- Generating weights to update alias linearly')

        # Generate weights to publish new alias linearly 
        weights = generate_weights(weight_function, steps)

        # Set func + alias to update
        func_name, alias_name = os.environ['function'], event['alias']
    
        for weight in weights:
            
            # Update new_version with weight value
            update_weight(lambda_client, func_name, alias_name, new_version, weight, log)
            
            # Wait interval secs to update weight
            time.sleep(interval)

            # Check there are no errors on new version 
            success = check_cloudwatch_errors(func_name, alias_name, new_version, log)
            
            # Rollback to original version if error
            if not success:
                rollback(lambda_client, func_name, alias_name, log)
                raise Exception('Errors on Cloudwatch, exiting')

        # Set the new version as the primary version and reset the AdditionalVersionWeights
        _ = finalize(lambda_client, func_name, alias_name, new_version)

        log.info(f'Info -- Alias {func_name}:{alias_name} is now routing 100% of traffic to version {new_version}')

        return json.dumps({"statusCode": 200, "body": "Canary Deploy is succesful"})
    
    else:

        # Return a 400 status if update not succesful
        return json.dumps({"statusCode": 400, "body": f"Canary Deploy is not succesful, Status: {status}, Message: {message}"})
    
